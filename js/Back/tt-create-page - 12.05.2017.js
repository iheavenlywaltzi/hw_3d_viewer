$(function() {
var objectBlock = {			
			'top':0, // расположение кнопки сверху
			'left':0, // расположение кнопки слева
			'show':0, // путь до картинки для обложки кнопки
			'type':0, // тип кнопки ( какой объект создать на поле )
			'src':0, // ссылка на контент который нужно создать в объекте ( или сам контент )
			'rotary':false, // поворот создоваемого объекта ( если false будет использоваться поворот окна в котором кнопка )
			'position': {'top':0,'left':0}, // положение ново-созданного объекта на поле ( относительно окна в котором он )
			'border':0, // css свойство border 
			'back':0} // css свойство background


	var ob_1 = {
			'top':31,
			'left':-80,
			'show':'img/icon/video-icon-test.png',
			'type':'content',
			'src':'Info text bla bla bla',
			'rotary':false,
			'position': {'top':320,'left':20},
			'border':0,
			'back':'none'
		}
		
	var ob_2 = {
			'top':-35,
			'left':-50,
			'show':'img/icon/img-icon-mini.png',
			'type':'img',
			'src':'img/kosmos18.jpg',
			'rotary':false,
			'position': {'top':153,'left':138},
			'border':0,
			'back':0
		}
		
	var ob_3 = {
			'top':-67,
			'left':42,
			'show':'img/icon/video-icon-mini.png',
			'type':'iframe',
			'src':'video/video_1.mp4',
			'rotary':false,
			'position': {'top':52,'left':382},
			'border':0,
			'back':0
		}
		
	var ob_4 = {
			'top':-35,
			'left':132,
			'show':'img/icon/3d-icon-mini.png',
			'type':'3d',
			'src':'3d/ab.json',
			'rotary':false,
			'position': {'top':140,'left':661},
			'border':0,
			'back':0
		}
		
	var ob_5 = {
			'top':31,
			'left':158,
			'show':'img/icon/television-icon-mini.png',
			'type':'background',
			'src':'img/bac_fon_1.gif',
			'rotary':false,
			'position': {'top':140,'left':661},
			'border':0,
			'back':0
		}
	
	var windowCreate = {
		'window_1':{
			'top':0,
			'left':0,
			'rotary':0,
			'objects':{
				'ob_1':ob_1,
				'ob_2':ob_2,
				'ob_3':ob_3,
				'ob_4':ob_4,
				'ob_5':ob_5
			}
		},
		'window_2':{
			'top':0,
			'left':959,
			'rotary':0,
			'objects':{
				'ob_1':ob_1,
				'ob_2':ob_2,
				'ob_3':ob_3,
				'ob_4':ob_4,
				'ob_5':ob_5
			}
		},
		'window_3':{
			'top':539,
			'left':0,
			'rotary':0,
			'objects':{
				'ob_1':ob_1,
				'ob_2':ob_2,
				'ob_3':ob_3,
				'ob_4':ob_4,
				'ob_5':ob_5
			}
		},
		'window_4':{
			'top':539,
			'left':959,
			'rotary':0,
			'objects':{
				'ob_1':ob_1,
				'ob_2':ob_2,
				'ob_3':ob_3,
				'ob_4':ob_4,
				'ob_5':ob_5
			}
		}
	};
	
var clWindow = windowControl.substr(1);
var clCentralBut = centralBut.substr(1);
var clCenButCli = cenButCli.substr(1);

var clCenMinBut = 'centr-mini-but';
var clCreConInfo = 'create-content-info';
var clButCreCon = butСС.substr(1);
var clCloContent = cloneContent.substr(1);

	$.each(windowCreate, function(){
		var allButton ='',newWindow;
		var winTop = (this.top)? this.top:0;
		var winLeft = (this.left)? this.left:0;
		var winRotary = (this.rotary)? this.rotary:0;
		
		if (rotaryL) {
			if (winTop < rotaryLine) {
				winRotary = 180;
			}
		}
		if (this.objects){
			$.each(this.objects,function(){
				
				var obTop = (this.top)? this.top : 0;
				var obLeft = (this.left)? this.left : 0;
				var obShow = (this.show)? this.show : 'img/icon/none-img.png';
				var obType = (this.type)? this.type : 'content';
				var obContent = '';
				var obSrc = (this.src)? 'data-src='+'"'+this.src+'"' : ''; 
				var obRotary = (this.rotary)? 'data-rotary="'+this.rotary+'"' : 'data-rotary="'+winRotary+'"';
				var obPosTop = (this.position.top)? 'data-top="'+this.position.top+'"' : 'data-top="0"';
				var obPosLeft = (this.position.left)? 'data-left="'+this.position.left+'"' : 'data-left="0"';
				var obBorder = (this.border)? 'data-border="'+this.border+'"' : '';
				var obBack = (this.back)? 'data-back="'+this.back+'"' : '';
				
				if (obType == 'content') {obContent = this.src; obSrc = '';}
				
				allButton = allButton+
					//'<div class="'+clCenMinBut+'" style="top:'+obTop+'px; left:'+obLeft+'px;">'+
					'<div class="'+clCenMinBut+'" style="top:'+obTop+'px; left:'+obLeft+'px;">'+
						'<div class="'+clButCreCon+'">'+
							'<div class="create-content-info">'+
								'<img src="'+obShow+'">'+
							'</div>'+
							'<div class="clone-content"'+
								'data-type="'+obType+'"'+
								obSrc+obRotary+obPosTop+obPosLeft+obBorder+obBack+
								'>'+
							obContent+
							'</div>'+							
						'</div>'+
					'</div>'+
					'';
			});	
		}
		
		newWindow =''+
			'<div class="'+clWindow+'" style="top:'+winTop+'px; left:'+winLeft+'px; transform:rotate('+winRotary+'deg);">'+
				'<div class="'+clCentralBut+'">'+
				'<div class="'+clCenButCli+'"></div>'+
					allButton+
				'</div>'+
			'</div>'+
		'';
		$(windo).append(newWindow);
	});
	

});

