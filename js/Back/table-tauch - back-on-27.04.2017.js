$(function() {

// $('#info2').html('x = '+x+'<br> y = '+y);

var winFrame = true; // если правда блок небудет выходить за рамки, своего родителя, если лож будет
var kinetic = true; // Физика движений для блоков, включить выключить.
	
var doc = document;
var block = '.block'; // класс или ид элемента который двигается
var blockTarget = '.block-target'; // класс или ид элемента с помощью которого двигаем
var blockDelete = '.block-delete'; // класс при клики на который удоляем блок
var targetOpen = '.open-control-menu'; // класс при клике на который раскрывается таргет меню
var butСС = '.but-create-content'; // класс из которого мы копируем контентв основное поле
var cloneContent = '.clone-content'; // все внутри этого блока клонировать на главный экран

var blockTypeObj = '.block-content-object'; // тип объекта - объект
var blockTypeImg = '.block-content-img'; // тип объекта - картинка
//var blockCotrolMenu = 'control-menu'; // класс добовляемый при 2йном клике (без точки)
var windo = '.content'; // клас или ид элемента окна, в котором движущиеся блоки

var targetCotrolH = 40; // высота окна таргета (должна быть как и в css) 

var timerDB = 200; // таймер между кликами (для дабл клика)
var timerKin = 200; // таймер для инерции (время в которое фиксируется растояние пройденное пальцем) 
var timerAnim = 400; // таймер Анимации физики элемента (время которое летит элемент после броска)
var timerAnimBut = 200; // таймер Анимации Кнопок (время раскрывания закрывания кнопок)


var minElemW = 180; // минимальная ширина элемента
var minElemH = 180; // минимальная высота элемента
var maxElemW = 900; // максимальная высота элемента
var maxElemH = 900; // максимальная высота элемента


var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
var gDataInfo = {}; // Объект с глобальными переменными по блокам

//function 
	// Animation 
	function animate(options) {
	  var start = performance.now();
	  requestAnimationFrame(function animate(time) {
		// timeFraction от 0 до 1
		var timeFraction = (time - start) / options.duration;
		if (timeFraction > 1) timeFraction = 1;
		// текущее состояние анимации
		var progress = options.timing(timeFraction)
		options.draw(progress);
		if (timeFraction < 1) {
		  requestAnimationFrame(animate);
		}
	  });
	}
		
	function circ(timeFraction) {
	  return 1 - Math.sin(Math.acos(timeFraction))
	}
	
	function makeEaseOut(timing) {
	  return function(timeFraction) {
		return 1 - timing(1 - timeFraction);
	  }
	}
		
	// Drag and Drob block
	function drag_block(data){
		var elem = data['elem']; // сам элемент
		var sElx = data['sElx']; // положение элемента при старте (топ)
		var sEly = data['sEly']; // положение элемента при старте (левт)
		var	elW = data['elW']; // ширина элемента
		var	elH = data['elH']; // высота элемента
		var sx = data['sx']; // положение пальца при клике (топ)
		var sy = data['sy']; // положение пальца при клике (лефт)
		var Ww = data['Ww']; // ширина главного окна
		var Wh = data['Wh']; // Высота главного окна
		var tx,ty,x,y,lastX=sx,lastY=sy,xMax,yMax,touchm;	
		var timeControl = new Date().getTime();
		var currentTime = new Date().getTime();
		
		var elemMove = data['blockElem']; // элемент который нужно двигать
		if (!elemMove){elemMove = elem;}
		
		$(elem).off('touchmove');
		$(elem).on('touchmove',function(event) { // если тянем 1 пальцем
			if (event.targetTouches.length == 1) { // 1 касание
				touchm = event.targetTouches[0];
				tx = touchm.pageX;
				ty = touchm.pageY;
				
				x = Math.round((tx-sx)+sElx);
				y = Math.round((ty-sy)+sEly);
				
				if (kinetic){
					currentTime = new Date().getTime();
					if (timeControl+timerKin <= currentTime) {
						mouseMove = 2;
						timeControl = currentTime;
						lastX = touchm.pageX;
						lastY = touchm.pageY;
					}
				}
				
				if (winFrame) {
					xMax = Math.round(Ww-elW);
					yMax = Math.round(Wh-elH);
					if (x >= xMax){x = xMax;} if (x <= 0) {x = 0;}
					if (y >= yMax){y = yMax;} if (y <= 0) {y = 0;}
				}
				
				$(elemMove).css({'left' : x+'px','top':y+'px'})
			}
		}),false;
		
		$(elem).off('touchend');
		$(elem).one('touchend',function(event) {
			$(elem).off('touchmove');
			if (kinetic){ // kinetic move
				var xdif,xk,ydif,yk,kofX,kofY,xm,ym;
				
				xdif = sx-sElx;
				lastX = lastX-xdif;
				xk = x-lastX;
				
				ydif = sy-sEly;
				lastY = lastY-ydif;
				yk = y-lastY;
				
				var distanceX = xk*2;
				var distanceY = yk*2;
				
				animate({
					duration:timerAnim,
					timing: makeEaseOut(circ),
					draw: function(prog){
						xm = x+(prog*distanceX);
						ym = y+(prog*distanceY);
						if (winFrame){
							if (xm >= xMax) {xm = xMax;} if (xm <= 0) {xm = 0;}
							if (ym >= yMax) {ym = yMax;} if (ym <= 0) {ym = 0;}
						}
						$(elemMove).css({'left':xm,'top':ym});
					}
				});
			}
		}),false;
	};
	
	// Resize Block
	function resize_block(data){
		var elem = data['elem'];
		var sx = data['sx'];
		var sy = data['sy'];
		var ew,eh,x,y,lx,ly;
		
		var elemMove = data['blockElem'];
		if (!elemMove){elemMove = elem;}
		
		$(elem).off('touchmove');
		$(elem).on('touchmove',function(event) { // если тянем 2мя пальцами
			if (event.targetTouches.length == 2) {
				var touchA = event.targetTouches[0];
				var touchB = event.targetTouches[1];
				ew = Math.round(Math.abs(touchA.pageX - touchB.pageX)-sx);
				eh = Math.round(Math.abs(touchA.pageY - touchB.pageY)-sy);
				
				x = ew+data['elW'];
				y = eh+data['elH'];
				lx = data['sElx']-(ew/2);
				ly = data['sEly']-(eh/2);
				
				if (x <= minElemW) {x = minElemW;} 
				if (y <= minElemH) {y = minElemH;} 

				$(elemMove).css({'height':y,'width':x});
				$(elemMove).css({'left' : lx+'px','top':ly+'px'});
			}
		}),false;
		
		$(elem).off('touchend');
		$(elem).one('touchend',function(event) {
			$(elem).off('touchmove');
			
		}),false;
	}
	
	// Rotary block
	function rotary_block(data){
		var elem = data['elem'];
		var sx = data['sx'];
		var sy = data['sy'];
		var ew,eh,x,y,rotate;
		var rotateO = 0;
		
		var elemMove = data['blockElem'];
		if (!elemMove){elemMove = elem;}
		
		$(elem).off('touchmove');
		$(elem).on('touchmove',function(event) { // если тянем 2мя пальцами
			if (event.targetTouches.length >= 3) {
				var touchA = event.targetTouches[0];
				var touchB = event.targetTouches[1];
				ew = Math.round(Math.abs(touchA.pageX - touchB.pageX)-sx);
				eh = Math.round(Math.abs(touchA.pageY - touchB.pageY)-sy);
				
				x = ew+data['elW'];
				y = eh+data['elH'];
				rotate = x-y;
				
				//elem.rotate(rotate);
				if (rotate >= 90) {rotateO = 0;}
				if (rotate <= 90) {rotateO = 180;}
				if (rotate >= -90) {rotateO = 0;}
				if (rotate <= -90) {rotateO = 180;}
				$('#info1').html('rotate = '+rotate+'<br> y = '+y)
				$(elemMove).css({'transform': 'rotate('+rotate+'deg)'});
				//$(elem).animate({'transform': 'rotate('+rotateO+'deg)'});
				//$(elem).css({'left' : lx+'px','top':ly+'px'});
			}
		}),false;
		
		$(elem).off('touchend');
		$(elem).one('touchend',function(event) {
			$(elem).off('touchmove');
			
			$(elemMove).css({'transform': 'rotate('+rotateO+'deg)'});
			
		}),false;
	}
	
	
	// touch block
	$(windo).on('touchstart',blockTarget,function(event) { 
		var timeStart = new Date().getTime(); // время клика на объект
		var tt = event.targetTouches.length; // колочество пальцев на объекте
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(block); // получим основной движемый элемент
		$(blockElem).stop();// остановим анимацию при клике
		var index = $(blockElem).index();
		// Element
		var elW = blockElem.width(); // ширина элемента
		var elH = blockElem.height(); // Высота элемента
		var elXY = blockElem.position();
		var sElx = elXY.left; // расположение ЛЕФТ элемента
		var sEly = elXY.top; // расположение  ТОП элемент
		// Main Window
		var panel = $(blockElem).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна

		var data = {};
		data = {
			'elW':elW,
			'elH':elH,
			'sElx':sElx,
			'sEly':sEly,
			'elem':elem,
			'blockElem':blockElem,
			'Ww':Ww,
			'Wh':Wh,
		};
		
		if (tt == 1) {
			var GlobObject = gDataInfo['block_'+index];
			if (GlobObject){
				if (GlobObject.startTime+timerDB >= timeStart) {//двойной клик
					var heightTE = $(elem).css('height');
					if (heightTE == targetCotrolH+'px'){
						$(elem).animate({"height": 100+'%'},timerAnim);
						$(elem).children(targetOpen).html('&uarr;');
					}
					else {
						$(elem).animate({"height": targetCotrolH+'px'},timerAnim);
						$(elem).children(targetOpen).html('&darr;');
					}
				}
				gDataInfo['block_'+index] = {'startTime':timeStart};
			}
			else {gDataInfo['block_'+index] = {'startTime':timeStart};}
				
			var touch = event.targetTouches[0];
			data['sx'] = touch.pageX;
			data['sy'] = touch.pageY;
			
			drag_block(data);
		}
		
		else if (tt == 2) {
			var touchSA = event.targetTouches[0];
			var touchSB = event.targetTouches[1];
			data['sx'] = Math.round(Math.abs(touchSA.pageX - touchSB.pageX));
			data['sy'] = Math.round(Math.abs(touchSA.pageY - touchSB.pageY));
			
			resize_block(data);
		}

		else if (tt >= 3) {	
			var touchSA = event.targetTouches[0];
			var touchSB = event.targetTouches[1];
			data['sx'] = Math.round(Math.abs(touchSA.pageX - touchSB.pageX));
			data['sy'] = Math.round(Math.abs(touchSA.pageY - touchSB.pageY));
			
			rotary_block(data);
		}
	}),false;
	
	// click Block
	$(windo).on('mousedown','.block-target',function(event) {
		var timeStart = new Date().getTime(); // время клика на объект
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(block); // получим основной движемый элемент	
		var index = $(blockElem).index();
		// Element
		var elW = blockElem.width(); // ширина элемента
		var elH = blockElem.height(); // Высота элемента
		var elXY = blockElem.position();
		var sElx = elXY.left; // расположение ЛЕФТ элемента
		var sEly = elXY.top; // расположение  ТОП элемент
		// Main Window
		var panel = $(blockElem).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна
		var tx,ty,x,y,lastX,lastY,currentTime=timeStart,timeControl=timeStart;
		var xMax = Math.round(Ww-elW);
		var yMax = Math.round(Wh-elH);
				
		var data = {};
		data = {
			'elW':elW,
			'elH':elH,
			'sElx':sElx,
			'sEly':sEly,
			'elem':elem,
			'blockElem':blockElem,
			'Ww':Ww,
			'Wh':Wh,
		};	
		
		var GlobObject = gDataInfo['block_'+index];
		if (GlobObject){
			if (GlobObject.startTime+timerDB >= timeStart) {//двойной клик
				var heightTE = $(elem).css('height');
				if (heightTE == targetCotrolH+'px'){
					$(elem).animate({"height": 100+'%'},timerAnim);
					$(elem).children(targetOpen).html('&uarr;');
				}
				else {
					$(elem).animate({"height": targetCotrolH+'px'},timerAnim);
					$(elem).children(targetOpen).html('&darr;');
				}
			}
			gDataInfo['block_'+index] = {'startTime':timeStart};
		}
		else {gDataInfo['block_'+index] = {'startTime':timeStart};}		

		blockElem.addClass('active-mov');
		var sx = event.clientX;
		var sy = event.clientY;
		lastX = sx;
		lastY = sy;
		
		doc.onmousemove = function(event) { // если водим мышью
			tx = event.clientX;
			ty = event.clientY
			
			x = Math.round((tx-sx)+sElx);
			y = Math.round((ty-sy)+sEly);
			
			if (kinetic){
				currentTime = new Date().getTime();
				if (timeControl+timerKin <= currentTime) {
					mouseMove = 2;
					timeControl = currentTime;
					lastX = tx;
					lastY = ty;
				}
			}
			
			if (winFrame) {
				xMax = Math.round(Ww-elW);
				yMax = Math.round(Wh-elH);
				if (x >= xMax){x = xMax;} if (x <= 0) {x = 0;}
				if (y >= yMax){y = yMax;} if (y <= 0) {y = 0;}
			}
			
			$(blockElem).css({'left' : x+'px','top':y+'px'})
		}
		
		this.onmouseup = function(event) { // если отпустили мыш
			doc.onmousemove = null;
			$(elem).onmousemove = null;
			if (kinetic){ // kinetic move
				var xdif,xk,ydif,yk;
				var timeUp = new Date().getTime();
				
				xdif = sx-sElx;
				lastX = lastX-xdif;
				xk = x-lastX;
				
				ydif = sy-sEly;
				lastY = lastY-ydif;
				yk = y-lastY;
				
				var distanceX = xk;
				var distanceY = yk;
				
				var kof,kofX,kofY,xm,ym;
				
				//if (timeUp+200 <= currentTime){
					animate({
						duration:timerAnim,
						timing: makeEaseOut(circ),
						draw: function(prog){
							xm = x+(prog*distanceX);
							ym = y+(prog*distanceY);
							if (winFrame){
								if (xm >= xMax) {xm = xMax;} if (xm <= 0) {xm = 0;}
								if (ym >= yMax) {ym = yMax;} if (ym <= 0) {ym = 0;}
							}
							$(blockElem).css({'left':xm,'top':ym});
						}
					});
				//}
			}			
			blockElem.removeClass('active-mov');
		}
		doc.onmouseup = function(event) {
			doc.onmousemove = null;
			$(elem).onmousemove = null;
		}
	});
	
	// Delete block
	$(windo).on('click touchstart',blockDelete,function(event) {
		$(this).closest(block).remove();
	});
	
	// Open Control Menu
	$(windo).on('click touchstart',targetOpen,function(event) {
		var elem = $(this).closest(blockTarget); // получим основной движемый элемент	
		var heightTE = $(elem).css('height');	
		if (heightTE == targetCotrolH+'px') {
			$(elem).animate({"height": 100+'%'},timerAnimBut);
			$(this).html('&uarr;');
		}
		else {
			$(elem).animate({"height": targetCotrolH+'px'},timerAnimBut);
			$(this).html('&darr;');
		}
	});
	
	$(butСС).on('click touchstart', function(event) {
		var cloneInfo = $(this).find(cloneContent);
		var dataInfo = cloneInfo.data();
		var elemTop = 0,elemLeft = 0,pastContent,cloneElem;
		
		if (dataInfo.position) {
			elemTop = (dataInfo.position.top) ? dataInfo.position.top:0;
			elemLeft = (dataInfo.position.left) ? dataInfo.position.left:0;
		}
		
		if (dataInfo.object) {
			if (dataInfo.object == 'content') {
				pastContent = $(this).find(cloneInfo).html();
			}
			if (dataInfo.object == 'img') {
				if (dataInfo.src) {
					pastContent = '<img src="'+dataInfo.src+'">';
				}
			}
			if (dataInfo.object == 'iframe') {
				if (dataInfo.src) {
					pastContent = '<iframe src="'+dataInfo.src+'"></iframe>';
				}
			}
		}
		//if (cloneInfo.data('position').top) {}
		
		cloneElem = '<div class="block" style="top:'+elemTop+'px; left:'+elemLeft+'px;">\
			<div class="block-target">\
				<div class="open-control-menu">&darr;</div>\
				<div class="block-delete">&times;</div>\
			</div>\
			<div class="blok-content">\
		'+pastContent+'</div></div>';
		$(windo).append(cloneElem);
	});
	
});

