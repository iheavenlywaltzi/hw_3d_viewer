"use strict"

// Animation 
function animate(options) { // функция анимации
  var start = performance.now();
  var progress;
  requestAnimationFrame(function animate(time) {
	// timeFraction от 0 до 1
	var timeFraction = (time - start) / options.duration;
	if (timeFraction > 1) timeFraction = 1;
	// текущее состояние анимации
	if (options.timing){progress = options.timing(timeFraction);}
	else {progress = timeFraction;}
	options.draw(progress);
	if (timeFraction < 1) {
	  requestAnimationFrame(animate);
	}
  });
}
	
function circ(timeFraction) { // расчет планости анимацти
  return 1 - Math.sin(Math.acos(timeFraction))
}

function makeEaseOut(timing) { // обратная анимация
  return function(timeFraction) {
	return 1 - timing(1 - timeFraction);
  }
}

// функция возврата к 3Д после открытия окна доп информации
function back_3D(elem){
	$(elem).off('touchmove click touchstart');
	var glblocWindow = $(elem).closest(blokContent);
	var winLocking = $(glblocWindow).find(BCLocking); // окно блокирующее управление 3Д
	var window3d = $(glblocWindow).find(canvas3d); // окно с 3д элементом
	var windowDop = $(glblocWindow).find(BCDopWindow)[0]; // окно с информацией
	var win3dleftI = ($(window3d).css('left').substr(0,1) == '-') ?'-':'';// определим "+" или "-" 
	//var stWindowDop = ($(windowDop).css('left') != '0px') ?'left':'right';// определим позицию информационного окна
	var w3d,wel,Xfactor,XfactorF;;
	animate({
		duration:timerAnim,
		draw: function(prog){
			prog = (1-prog)
			w3d = (prog)*widthDopInf;
			wel = 100 - w3d;
			Xfactor = blurBCLocking*(prog);
			XfactorF = opBCLocking*(prog);
			$(winLocking).css({'background': 'rgba(133, 166, 195, '+XfactorF+')'});
			$(window3d).css({'left':win3dleftI+w3d+'%','filter': 'blur('+Xfactor+'px)'});
			if (win3dleftI == '-'){windowDop.style.left = wel+'%';}
			else {windowDop.style.right = wel+'%';}
		}
	});
	setTimeout(function () {$(windowDop).remove();},timerAnim);
	setTimeout(function () {$(winLocking).remove();},timerAnim);
}

function Load_Canvas_3D (idName,JsonObj) { // функция загрузки 3д Элементов на страницу
	b4w.register(idName, function(exports, require) { 
	var m_main      = require("main");
	var m_app       = require("app");
	var m_data      = require("data");
	var m_preloader = require("preloader");

	var m_cont      = require("container");
	var m_scenes    = require("scenes");
	var m_trans = require("transform");
	var m_vec3 = require("vec3");
	var elContentBlock,elCanvas3d;

		exports.init = function() { 
			m_app.init({ 
			canvas_container_id: idName, 
			callback: init_cb, 
			autoresize: true 
			}); 
		} 
		
		exports.unload_cb = function() {
			m_main.pause();
			m_main.resume();
			m_data.cleanup();// функция выгрузки сцены
		}
		
		exports.set_render_callback = function(callback) {
			set_render_callback(callback);
			console.log('1');
			add_anchor();
		}
		
		function init_cb () { // функция загрузки сцены
			m_preloader.create_preloader(); // шкала загрузки ( создаем )
			m_data.load(JsonObj, load_cb,preloader_cb);  // загружаем json объект
		} 
		
		function load_cb() { // если объект загружен выполняем эту функцию
			
			if (m_scenes.check_object_by_name(objCon3dButAntiFatal)) { // если объект есть в сцене
				var canvas_elem = m_cont.get_canvas(); // получим канвас в котором 3д
				canvas_elem.addEventListener("mousedown", main_canvas_click, false);
				canvas_elem.addEventListener("touchstart", main_canvas_touch, false);
			}
			//m_main.set_render_callback(frame_cb); // функция котороя вызывается каждый кадр
			m_app.enable_camera_controls(); // функция контроля камеры
		}
		
		function frame_cb(){ // функция рассчета по ФПС
			console.log('1');
		}
		
		function preloader_cb(percentage) { // шкала загрузки
			m_preloader.update_preloader(percentage); // шкала загрузки ( инициализируем )
		}
		
		function object_operation (obj,objCon3dBut,x=0) {
			var controlLoR = elContentBlock[0].offsetWidth / 2;
			var addElemHtml;
			var LeftOrRight,dopWindowLoc,dopWindow,w3d,wel,lorPos,posDrag,Xfactor,XfactorF;
			$.each(objCon3dBut, function(index){
				if (obj.name == index){
					addElemHtml = '<div class="canvas3d_dop_block '+conInfoWin.substr(1)+'">'+
						'<div class="'+conInfoMove.substr(1)+'">'+
							this+
						'</div>'+
					'<div class="'+back3d.substr(1)+' '+back3dBut.substr(1)+'">Вернуться к 3D</div>'+
					'</div>';
				}
			});
			
			LeftOrRight = (x < controlLoR) ? 'left' : 'right';
			
			if (LeftOrRight == 'left') {
				lorPos = '';
				posDrag = 'right';
			}
			if (LeftOrRight == 'right') {
				lorPos = '-';
				posDrag = 'left';
			}	
									
			dopWindowLoc = '<div class="'+BCLocking.substr(1)+' '+back3d.substr(1)+'"> </div>';
			dopWindow = '<div class="'+BCDopWindow.substr(1)+'" style="'+posDrag+':100%; width:'+widthDopInf+'%">'+addElemHtml+'</div>'
			$(elContentBlock).append(dopWindowLoc+dopWindow);
			var dopWindow = $(elContentBlock).find(BCDopWindow);
			var dopWindowLoc = $(elContentBlock).find(BCLocking);
			
			animate({
				duration:timerAnim,
				draw: function(prog){
					w3d = (prog*widthDopInf);
					wel = 100 - w3d;
					Xfactor = blurBCLocking*prog;
					XfactorF = opBCLocking*prog;
					$(dopWindowLoc).css({'background': 'rgba(133, 166, 195, '+XfactorF+')'});
					$(elCanvas3d).css({'left':lorPos+w3d+'%','filter': 'blur('+Xfactor+'px)'});
					(posDrag == 'left')? $(dopWindow).css({'left':wel+'%'}):'';
					(posDrag == 'right')? $(dopWindow).css({'right':wel+'%'}):'';
				}
			});
		}
		
		function main_canvas_click(e) { // если кликнули мышью по конвасу
			if (e.preventDefault) e.preventDefault();
			var y = e.offsetY;
			var x = e.offsetX;
			elContentBlock = $(this).closest(blokContent);//получим блок куда вставим хтм посел нажатия
			elCanvas3d = $(elContentBlock).find(canvas3d);////блок внутри которого 3Д объет
			var obj = m_scenes.pick_object(x, y); // получим объект под курсором
			if (obj) { // если под курсором есть объект
				object_operation(obj,objCon3dBut,x);
			}
		}
		
		function main_canvas_touch(e) { // если нажали пальцем по канвасу (тачь)
			if (e.preventDefault) e.preventDefault();
			elContentBlock = $(this).closest(blokContent);//получим блок куда вставим хтм посел нажатия
			var controlLoR = elContentBlock[0].offsetWidth / 2;
			elCanvas3d = $(elContentBlock).find(canvas3d);////блок внутри которого 3Д объет
			var Ttop = $(this).closest(block).position().top;
			var Tleft = $(this).closest(block).position().left;
			var elW = $(this).closest(block).width(); // ширина элемента
			var elH = $(this).closest(block).height(); // Высота элемента
			var rotat = detect_rotate($(this).closest(rotaryBlock).css('transform'));
						
			if (rotat == 0){
				// расчетаем попали мы в объект или нет
				var y = e.targetTouches[0].pageY-Ttop;
				var x = e.targetTouches[0].pageX-Tleft;
			}
			if (rotat == 180) {
				// расчетаем попали мы в объект или нет
				var y = elH-(e.targetTouches[0].pageY-Ttop);
				var x = elW-(e.targetTouches[0].pageX-Tleft);
			}
			
			var obj = m_scenes.pick_object(x, y); // получим объект под курсором
			if (obj) { // если под курсором есть объект			
				object_operation(obj,objCon3dBut,x);
			}
		}
		

		
		function add_anchor(){
			var test;
			
			var obj = m_scenes.get_object_by_name(objCon3dButAntiFatal);
			var cam = m_scenes.get_active_camera();
			var pos = m_trans.get_translation(obj, test);
			//m_vec3.add(pos, anchor_offset, pos);
			console.log('pos = '+pos);
			console.log('test = '+test);
		}
	});
	
	b4w.require(idName,idName).init();
}

// convert matrix at deg. переведет полученую матрицу в угол поворота
function detect_rotate (Rotat) {
	var values = Rotat.split('(')[1].split(')')[0].split(',');
	var a = values[0];
	var b = values[1];
	var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
	return angle;
}

// Drag and Drob block
function drag_block(data){ // функция движения блока
	var elem = data['elem']; // сам элемент
	var sElx = data['sElx']; // положение элемента при старте (топ)
	var sEly = data['sEly']; // положение элемента при старте (левт)
	var	elW = data['elW']; // ширина элемента
	var	elH = data['elH']; // высота элемента
	var sx = data['sx']; // положение пальца при клике (топ)
	var sy = data['sy']; // положение пальца при клике (лефт)
	var Ww = data['Ww']; // ширина главного окна
	var Wh = data['Wh']; // Высота главного окна
	var typeEvent = data['typeEvent']; // Высота главного окна
	var resizeControl = data['resizeControl'];
	var tx,ty,x,y,lastX=sx,lastY=sy,xMax,yMax,touchm;	
	var timeControl = new Date().getTime();
	var currentTime = new Date().getTime();

	var elemMove = data['blockElem']; // элемент который нужно двигать
	if (!elemMove){elemMove = elem;}
	
	function drag(){
		x = Math.round((tx-sx)+sElx);
		y = Math.round((ty-sy)+sEly);
		
		if (kinetic){
			currentTime = new Date().getTime();
			if (timeControl+timerKin <= currentTime) {
				timeControl = currentTime;
				lastX = tx;
				lastY = ty;
			}
		}
		
		if (winFrame) {
			xMax = Math.round(Ww-elW);
			yMax = Math.round(Wh-elH);
			if (x >= xMax){x = xMax;} if (x <= 0) {x = 0;}
			if (y >= yMax){y = yMax;} if (y <= 0) {y = 0;}
		}
		
		$(elemMove).css({'left' : x+'px','top':y+'px'})
	}
	
	if (typeEvent == 'touch') {
		$(elem).off('touchmove');
		$(elem).on('touchmove',function(event) { // если тянем 1 пальцем
			if (event.targetTouches.length == 1) { // 1 касание
				touchm = event.targetTouches[0];
				tx = touchm.pageX;
				ty = touchm.pageY;
				drag();
			}
		}),false;

	}
	if (typeEvent == 'mouse') { 
		$(elem).off('mousemove');
		doc.onmousemove = function(event) {
			tx = event.clientX;
			ty = event.clientY
			drag();
		}
	}
	
	$(elem).off('touchend');$(elem).off('mouseup');
	$(elem).one('touchend mouseup',function(event) {
		$(elem).off('touchmove');$(elem).off('mousemove');
		doc.onmousemove = null;
		$(elem).onmousemove = null;
		var yRotaryControl = y;
		if (kinetic){ // kinetic move
			var xdif,xk,ydif,yk,kofX,kofY,xm,ym;
			var timeUp = new Date().getTime();
			
			xdif = sx-sElx;
			lastX = lastX-xdif;
			xk = x-lastX;
			
			ydif = sy-sEly;
			lastY = lastY-ydif;
			yk = y-lastY;
			
			var distanceX = xk*kofDis;
			var distanceY = yk*kofDis;
			
			//if (timeUp <= currentTime+200 && x < xMax && y < yMax && x > 0 && y > 0){ // отключение  отскакивания
			if (timeUp <= currentTime+200){
				animate({
					duration:timerAnim,
					timing: makeEaseOut(circ),
					draw: function(prog){
						xm = x+(prog*distanceX);
						ym = y+(prog*distanceY);
						if (winFrame){
							if (xm >= xMax) {xm = xMax;} if (xm <= 0) {xm = 0;}
							if (ym >= yMax) {ym = yMax;} if (ym <= 0) {ym = 0;}
						}
						$(elemMove).css({'left':xm,'top':ym});
					}
				});
			}
			yRotaryControl = y+distanceY;
		}
		if (rotaryL) {
			if (!yRotaryControl) {yRotaryControl = sEly;}
			if (yRotaryControl+(elH/2) >= rotaryLine) {
				if (resizeControl) {$(elem).find(rotaryBlock).css({'transform':'rotate('+rotaryHoriz+'deg)'});}
				else {$(elem).closest(rotaryBlock).css({'transform':'rotate('+rotaryHoriz+'deg)'});}
			}else {
				if (resizeControl) {$(elem).find(rotaryBlock).css({'transform':'rotate('+rotaryNone+'deg)'});}
				else {$(elem).closest(rotaryBlock).css({'transform':'rotate('+rotaryNone+'deg)'});}
			}
		}
	}),false;
}

// Resize Block
function resize_block(data){ // функция изменения размеров блока
	var elem = data['elem'];
	var sx = data['sx'];
	var sy = data['sy'];
	var Ww = data['Ww']; // ширина главного окна
	var Wh = data['Wh']; // Высота главного окна
	var contentBlock = data['contentBlock'];
	var ew,eh,x,y,lx,ly,xMax,yMax,kofRes,resizeD;
	
	var elemMove = data['blockElem'];
	if (!elemMove){elemMove = elem;}
	
	
	if (contentBlock == 'img') {
		//kofRes = data['elH'] / data['elW'];
		//maxElemH = maxElemH*kofRes;
	}
	
	$(elem).off('touchmove');
	$(elem).on('touchmove',function(event) { // если тянем 2мя пальцами
		if (event.targetTouches.length == 2) {
			var touchA = event.targetTouches[0];
			var touchB = event.targetTouches[1];
			ew = Math.round(Math.abs(touchA.pageX - touchB.pageX)-sx);
			eh = Math.round(Math.abs(touchA.pageY - touchB.pageY)-sy);
			
			if (contentBlock == 'img') {
				resizeD = (eh+ew);
				x = resizeD+data['elW'];
			}
			else {y = eh+data['elH']; x = ew+data['elW'];}
			
			lx = data['sElx']-(ew/2);
			ly = data['sEly']-(eh/2);
							
			if (x <= minElemW) {x = minElemW;} if (x >= maxElemW) {x = maxElemW;}
			if (y <= minElemH) {y = minElemH;} if (y >= maxElemH) {y = maxElemH;}
			
			xMax = Math.round(Ww-lx);
			yMax = Math.round(Wh-ly);

			if (lx <= 0) {lx = 0;} if (x >= xMax) {x = xMax;}
			if (ly <= 0) {ly = 0;} if (y >= yMax) {y = yMax;}

			$(elemMove).css({'height':y,'width':x});
			$(elemMove).css({'left' : lx+'px','top':ly+'px'});
		}
	}),false;
	
	$(elem).off('touchend');
	$(elem).one('touchend',function(event) {
		$(elem).off('touchmove');
		
	}),false;
}

function create_page(windowCreate) { // функция создания страници
	$.each(windowCreate, function(){
		var allButton ='',newWindow;
		var winTop = (this.top)? this.top:0;
		var winLeft = (this.left)? this.left:0;
		var winRotary = (this.rotary)? this.rotary:0;
		var clControlBut;
		
		if (rotaryL) {
			if (winTop < rotaryLine) {
				winRotary = 180;
			}
		}
		if (this.objects){
			$.each(this.objects,function(){
				
				var obTop = (this.top)? this.top : 0;
				var obLeft = (this.left)? this.left : 0;
				var obShow = (this.show)? this.show : 'img/icon/none-img.png';
				var obType = (this.type)? this.type : 'content';
				var obContent = '';
				var obSrc = (this.src)? 'data-src='+'"'+this.src+'"' : ''; 
				var obRotary = (this.rotary)? 'data-rotary="'+this.rotary+'"' : 'data-rotary="'+winRotary+'"';
				if (this.position) {
					var obPosTop = (this.position.top)? 'data-top="'+this.position.top+'"' : 'data-top="0"';
					var obPosLeft = (this.position.left)? 'data-left="'+this.position.left+'"' : 'data-left="0"';
				} else {obPosTop = ''; obPosLeft='';}
				var obBorder = (this.border)? 'data-border="'+this.border+'"' : '';
				var obBack = (this.back)? 'data-back="'+this.back+'"' : '';
				
				if (obType == 'content') {obContent = this.src; obSrc = '';}
				if (obType == 'background') {clControlBut = butBС.substr(1);} 
				else {clControlBut = butСС.substr(1);}
				
				allButton = allButton+
					//'<div class="'+clCenMinBut+'" style="top:'+obTop+'px; left:'+obLeft+'px;">'+
					'<div class="'+clCenMinBut.substr(1)+'" style="top:'+obTop+'px; left:'+obLeft+'px;">'+
						'<div class="'+clControlBut+'">'+
							'<div class="'+clCreConInfo.substr(1)+'">'+
								obShow+
							'</div>'+
							'<div class="'+cloneContent.substr(1)+'"'+
								'data-type="'+obType+'"'+
								obSrc+obRotary+obPosTop+obPosLeft+obBorder+obBack+
								'>'+
							obContent+
							'</div>'+							
						'</div>'+
					'</div>'+
					'';
			});	
		}
		
		newWindow =''+
			'<div class="'+windowControl.substr(1)+'" style="top:'+winTop+'px; left:'+winLeft+'px; transform:rotate('+winRotary+'deg);">'+
				'<div class="'+centralBut.substr(1)+'" style="bottom:'+dropUp+'px">'+
				'<div class="'+cenButCli.substr(1)+'"></div>'+
					allButton+
				'</div>'+
			'</div>'+
		'';
		$(windo).append(newWindow);
	});
}	
