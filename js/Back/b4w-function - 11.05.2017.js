
"use strict"

function Load_Canvas_3D (idName,JsonObj) {
	b4w.register(idName, function(exports, require) { 
	var m_app       = require("app");
	var m_data      = require("data");
	var m_preloader = require("preloader");
	  
		exports.init = function() { 
			m_app.init({ 
			canvas_container_id: idName, 
			callback: init_cb, 
			autoresize: true 
			}); 
		} 
		
		function init_cb () { 
			m_preloader.create_preloader(); // шкала загрузки ( создаем )
			m_data.load(JsonObj, load_cb,preloader_cb); 
		} 
		
		function load_cb() {
			m_app.enable_camera_controls();
		}
		
		function preloader_cb(percentage) {
			m_preloader.update_preloader(percentage); // шкала загрузки ( инициализируем )
		}
	});
	
	b4w.require(idName,idName).init();
}

function Unload_Canvas(id) {
	b4w.cleanup(id,id);
}
