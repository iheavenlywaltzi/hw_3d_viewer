"use strict"
//disable touch отключение действий по умолчанию на таче 
function disable_touch(){
	//var doc = document;
	//var windo = '.content'; // клас или ид элемента окна, в котором движущиеся блоки
	// отключить браузерные функции перетаскивания	
	doc.ondragstart = function() {
	  return false;
	};
	// отменяем тачь действия браузера по умолчанию.
	$(disableTouchClass).on('touchstart touchmove touchend', function(event) {
		event.preventDefault(); // Отменяет событие, если оно отменяемое
		event.stopPropagation(); // Прекращает дальнейшую передачу текущего события.
	});	
}

// Animation 
function animate(options) { // функция анимации
  var start = performance.now();
  var progress;
  requestAnimationFrame(function animate(time) {
	// timeFraction от 0 до 1
	var timeFraction = (time - start) / options.duration;
	if (timeFraction > 1) timeFraction = 1;
	// текущее состояние анимации
	if (options.timing){progress = options.timing(timeFraction);}
	else {progress = timeFraction;}
	options.draw(progress);
	if (timeFraction < 1) {
	  requestAnimationFrame(animate);
	}
  });
}
	
function circ(timeFraction) { // расчет планости анимацти
  return 1 - Math.sin(Math.acos(timeFraction))
}

function makeEaseOut(timing) { // обратная анимация
  return function(timeFraction) {
	return 1 - timing(1 - timeFraction);
  }
}

// функция возврата к 3Д после открытия окна доп информации
function back_3D(elem){
	var timeControl = new Date().getTime(); // время клика на объект
	var glblocWindow = $(elem).closest(bloсkContent);
	var winLocking = $(glblocWindow).find(BCLocking); // окно блокирующее управление 3Д
	var window3d = $(glblocWindow).find(canvas3d); // окно с 3д элементом
	var idCanvas3d = window3d.attr('id');
	var windowDop = $(glblocWindow).find(BCDopWindow)[0]; // окно с информацией
	var win3dleftI = ($(window3d).css('left').substr(0,1) == '-') ?'-':'';// определим "+" или "-" 
	var w3d,wel,Xfactor,XfactorF;
	
	if (timeControl-gDataInfo['canvas3d_'+idCanvas3d] > timerAnim+500) {
		gDataInfo['canvas3d_'+idCanvas3d] = timeControl;
		animate({
			duration:timerAnim,
			draw: function(prog){
				prog = (1-prog)
				w3d = (prog)*widthDopInf;
				wel = 100 - w3d;
				Xfactor = blurBCLocking*(prog);
				XfactorF = opBCLocking*(prog);
				$(winLocking).css({'background': 'rgba(133, 166, 195, '+XfactorF+')'});
				$(window3d).css({'left':win3dleftI+w3d+'%','filter': 'blur('+Xfactor+'px)'});
				if (win3dleftI == '-'){windowDop.style.left = wel+'%';}
				else {windowDop.style.right = wel+'%';}
			}
		});
		setTimeout(function () {$(windowDop).remove();},timerAnim);
		setTimeout(function () {$(winLocking).remove();},timerAnim);
	}else {}
}

function Load_Canvas_3D (idName,JsonObj) { // функция загрузки 3д Элементов на страницу
	b4w.register(idName, function(exports, require) { 
	var m_main      = require("main");
	var m_app       = require("app");
	var m_data      = require("data");
	var m_preloader = require("preloader");

	var m_cont      = require("container");
	var m_scenes    = require("scenes");
	var m_trans = require("transform");
	var m_vec3 = require("vec3");
	var block3d = $('#'+idName);
	var prelod_path = $(block3d).find('.preloader3d'); // сам контейнер загрузки
	var prelod_dynamic_path = $(block3d).find('.preloader3d').find('span');// полоса загрузки
	var prelod_text_path = $(block3d).find('.preloader3d').find('h4');// поле для текста загрузки
	var elContentBlock,elCanvas3d;

		exports.init = function() { 
			m_app.init({ 
			canvas_container_id: idName, 
			callback: init_cb, 
			autoresize: true 
			}); 
		} 
		
		exports.unload_cb = function() {
			m_main.pause();
			m_main.resume();
			m_data.cleanup();// функция выгрузки сцены
		}
		
		exports.load_3d = function(elem3dcontent){
			m_data.load(elem3dcontent, load_cb,preloader_cb);  // загружаем json объект
		}
		
		function init_cb () { // функция загрузки сцены
			m_data.load(JsonObj, load_cb,preloader_cb);  // загружаем json объект
		} 
		
		function load_cb() { // если объект загружен выполняем эту функцию
			
			if (m_scenes.check_object_by_name(objCon3dButAntiFatal)) { // если объект есть в сцене
				var canvas_elem = m_cont.get_canvas(); // получим канвас в котором 3д
				canvas_elem.addEventListener("mousedown", main_canvas_click, false);
				canvas_elem.addEventListener("touchstart", main_canvas_touch, false);
			}
			//m_main.set_render_callback(frame_cb); // функция котороя вызывается каждый кадр
			m_app.enable_camera_controls(); // функция контроля камеры
		}
		
		function frame_cb(){ // функция рассчета по ФПС
		
		}
		
		function preloader_cb(percentage) { // шкала загрузки
			prelod_dynamic_path[0].style.width = percentage + "%";
			prelod_text_path[0].innerHTML = percentage + "%";	   
			if (percentage == 100) {
				prelod_path[0].style.display = 'none'
				return;
			}
		}
		
		function object_operation (obj,objCon3dBut,x=0) {
			var timeControl = new Date().getTime();
			var controlLoR = elContentBlock[0].offsetWidth / 2;
			var addElemHtml;
			var LeftOrRight,dopWindowLoc,dopWindow,w3d,wel,lorPos,posDrag,Xfactor,XfactorF,ObjectNameControl='';
			
			if (!gDataInfo['canvas3d_'+idName]) {gDataInfo['canvas3d_'+idName]=1;}
			
			if (timeControl-gDataInfo['canvas3d_'+idName]  > timerAnim+500) {
				gDataInfo['canvas3d_'+idName] = timeControl;
				if (view3dObjectName) {ObjectNameControl = obj.name}
			
				if (objCon3dBut[obj.name]){
					addElemHtml = '<div class="canvas3d_dop_block '+conInfoWin.substr(1)+'">'+
						'<div class="'+conInfoMove.substr(1)+'">'+ObjectNameControl+
							objCon3dBut[obj.name]+
						'</div>'+
					'<div class="'+back3d.substr(1)+' '+back3dBut.substr(1)+'">'+back3dInf+'</div>'+
					'</div>';
				}
				else {
					addElemHtml = '<div class="canvas3d_dop_block '+conInfoWin.substr(1)+'">'+
						'<div class="'+conInfoMove.substr(1)+'">'+
							'<p>No information ( '+obj.name+' )</p>'+
						'</div>'+
					'<div class="'+back3d.substr(1)+' '+back3dBut.substr(1)+'">'+back3dInf+'</div>'+
					'</div>';
				}			
				
				LeftOrRight = (x < controlLoR) ? 'left' : 'right';
				if (dopInf3dExit) {LeftOrRight = dopInf3dExit;}
				
				if (LeftOrRight == 'left') {
					lorPos = '';
					posDrag = 'right';
				}
				if (LeftOrRight == 'right') {
					lorPos = '-';
					posDrag = 'left';
				}	
										
				dopWindowLoc = '<div class="'+BCLocking.substr(1)+' '+back3d.substr(1)+'"> </div>';
				dopWindow = '<div class="'+BCDopWindow.substr(1)+'" style="'+posDrag+':100%; width:'+widthDopInf+'%">'+addElemHtml+'</div>'
				$(elContentBlock).append(dopWindowLoc+dopWindow);
				var dopWindow = $(elContentBlock).find(BCDopWindow);
				var dopWindowLoc = $(elContentBlock).find(BCLocking);
				
				animate({
					duration:timerAnim,
					draw: function(prog){
						w3d = (prog*widthDopInf);
						wel = 100 - w3d;
						Xfactor = blurBCLocking*prog;
						XfactorF = opBCLocking*prog;
						$(dopWindowLoc).css({'background': 'rgba(133, 166, 195, '+XfactorF+')'});
						$(elCanvas3d).css({'left':lorPos+w3d+'%','filter': 'blur('+Xfactor+'px)'});
						(posDrag == 'left')? $(dopWindow).css({'left':wel+'%'}):'';
						(posDrag == 'right')? $(dopWindow).css({'right':wel+'%'}):'';
					}
				});
			}
		}
		
		function main_canvas_click(e) { // если кликнули мышью по конвасу
			if (e.preventDefault) e.preventDefault();
			var y = e.offsetY;
			var x = e.offsetX;
			elContentBlock = $(this).closest(bloсkContent);//получим блок куда вставим хтм посел нажатия
			elCanvas3d = $(elContentBlock).find(canvas3d);////блок внутри которого 3Д объет
			var obj = m_scenes.pick_object(x, y); // получим объект под курсором
			if (obj) { // если под курсором есть объект
				object_operation(obj,objCon3dBut,x);
			}
		}
		
		function main_canvas_touch(e) { // если нажали пальцем по канвасу (тачь)
			if (e.preventDefault) e.preventDefault();
			var tt = e.targetTouches.length;
			var elem = this;
			var controlTouchM=1,controlTouchE=1,controlIt=0;
			if (tt == 1) {
				
				$(elem).off('touchmove');
				$(elem).on('touchmove',function(event) {
					controlIt++
					if (controlIt >= timer3dControlrandkof){
						controlTouchM = 2;
						controlIt = 0;
					}
				});
				
				$(elem).off('touchend');
				$(elem).on('touchend',function(event) {
					controlTouchE = 2;
				});
				
				if (!timer3dControlEnd){controlTouchE = 2;}
				
				if (timer3dControl) {
					setTimeout(function () {
					if (controlTouchM==1 && controlTouchE==2) {
						elContentBlock = $(elem).closest(bloсkContent);//получим блок куда вставим хтм посел нажатия
						var controlLoR = elContentBlock[0].offsetWidth / 2;
						elCanvas3d = $(elContentBlock).find(canvas3d);////блок внутри которого 3Д объет
						var Ttop = $(elem).closest(block).position().top;
						var Tleft = $(elem).closest(block).position().left;
						var elW = $(elem).closest(block).width(); // ширина элемента
						var elH = $(elem).closest(block).height(); // Высота элемента
						var rotat = detect_rotate($(elem).closest(rotaryBlock).css('transform'));
									
						if (rotat == 0){
							// расчетаем попали мы в объект или нет
							var y = e.targetTouches[0].pageY-Ttop;
							var x = e.targetTouches[0].pageX-Tleft;
						}
						if (rotat == 180) {
							// расчетаем попали мы в объект или нет
							var y = elH-(e.targetTouches[0].pageY-Ttop);
							var x = elW-(e.targetTouches[0].pageX-Tleft);
						}

						var obj = m_scenes.pick_object(x, y); // получим объект под курсором
						if (obj) { // если под курсором есть объект			
							object_operation(obj,objCon3dBut,x);
						}
					}
				},timeRandomClick);
				}
				else {
					elContentBlock = $(elem).closest(bloсkContent);//получим блок куда вставим хтм посел нажатия
					var controlLoR = elContentBlock[0].offsetWidth / 2;
					elCanvas3d = $(elContentBlock).find(canvas3d);////блок внутри которого 3Д объет
					var Ttop = $(elem).closest(block).position().top;
					var Tleft = $(elem).closest(block).position().left;
					var elW = $(elem).closest(block).width(); // ширина элемента
					var elH = $(elem).closest(block).height(); // Высота элемента
					var rotat = detect_rotate($(elem).closest(rotaryBlock).css('transform'));
								
					if (rotat == 0){
						// расчетаем попали мы в объект или нет
						var y = e.targetTouches[0].pageY-Ttop;
						var x = e.targetTouches[0].pageX-Tleft;
					}
					if (rotat == 180) {
						// расчетаем попали мы в объект или нет
						var y = elH-(e.targetTouches[0].pageY-Ttop);
						var x = elW-(e.targetTouches[0].pageX-Tleft);
					}

					var obj = m_scenes.pick_object(x, y); // получим объект под курсором
					if (obj) { // если под курсором есть объект			
						object_operation(obj,objCon3dBut,x);
					}
				}


	
			}
		}

		function add_anchor(){
			var test;
			var obj = m_scenes.get_object_by_name(objCon3dButAntiFatal);
			var cam = m_scenes.get_active_camera();
			var pos = m_trans.get_translation(obj, test);
		}
	});
	
	b4w.require(idName,idName).init();
}

// convert matrix at deg. переведет полученую матрицу в угол поворота
function detect_rotate (Rotat) {
	var values = Rotat.split('(')[1].split(')')[0].split(',');
	var a = values[0];
	var b = values[1];
	var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
	return angle;
}

// Drag and Drob block
function drag_block(data){ // функция движения блока
	var elem = data['elem']; // сам элемент
	var sElx = data['sElx']; // положение элемента при старте (топ)
	var sEly = data['sEly']; // положение элемента при старте (левт)
	var	elW = data['elW']; // ширина элемента
	var	elH = data['elH']; // высота элемента
	var sx = data['sx']; // положение пальца при клике (топ)
	var sy = data['sy']; // положение пальца при клике (лефт)
	var Ww = data['Ww']; // ширина главного окна
	var Wh = data['Wh']; // Высота главного окна
	var typeEvent = data['typeEvent']; // Высота главного окна
	var resizeControl = data['resizeControl'];
	var tx,ty,x,y,lastX=sx,lastY=sy,xMax,yMax,touchm;	
	var timeControl = new Date().getTime();
	var currentTime = new Date().getTime();

	var elemMove = data['blockElem']; // элемент который нужно двигать
	if (!elemMove){elemMove = elem;}
	
	function drag(){
		x = Math.round((tx-sx)+sElx);
		y = Math.round((ty-sy)+sEly);
		
		if (kinetic){
			currentTime = new Date().getTime();
			if (timeControl+timerKin <= currentTime) {
				timeControl = currentTime;
				lastX = tx;
				lastY = ty;
			}
		}
		
		if (winFrame) {
			xMax = Math.round(Ww-elW);
			yMax = Math.round(Wh-elH);
			if (x >= xMax){x = xMax;} if (x <= 0) {x = 0;}
			if (y >= yMax){y = yMax;} if (y <= 0) {y = 0;}
		}
		
		$(elemMove).css({'left' : x+'px','top':y+'px'})
	}
	
	if (typeEvent == 'touch') {
		$(elem).off('touchmove');
		$(elem).on('touchmove',function(event) { // если тянем 1 пальцем
			if (event.targetTouches.length == 1) { // 1 касание
				touchm = event.targetTouches[0];
				tx = touchm.pageX;
				ty = touchm.pageY;
				drag();
			}
		}),false;

	}
	if (typeEvent == 'mouse') { 
		$(elem).off('mousemove');
		doc.onmousemove = function(event) {
			tx = event.clientX;
			ty = event.clientY
			drag();
		}
	}
	
	$(elem).off('touchend');$(elem).off('mouseup');
	$(elem).one('touchend mouseup',function(event) {
		$(elem).off('touchmove');$(elem).off('mousemove');
		doc.onmousemove = null;
		$(elem).onmousemove = null;
		var yRotaryControl = y;
		if (kinetic){ // kinetic move
			var xdif,xk,ydif,yk,kofX,kofY,xm,ym;
			var timeUp = new Date().getTime();
			
			xdif = sx-sElx;
			lastX = lastX-xdif;
			xk = x-lastX;
			
			ydif = sy-sEly;
			lastY = lastY-ydif;
			yk = y-lastY;
			
			var distanceX = xk*kofDis;
			var distanceY = yk*kofDis;
			
			//if (timeUp <= currentTime+200 && x < xMax && y < yMax && x > 0 && y > 0){ // отключение  отскакивания
			if (timeUp <= currentTime+200){
				animate({
					duration:timerAnim,
					timing: makeEaseOut(circ),
					draw: function(prog){
						xm = x+(prog*distanceX);
						ym = y+(prog*distanceY);
						if (winFrame){
							if (xm >= xMax) {xm = xMax;} if (xm <= 0) {xm = 0;}
							if (ym >= yMax) {ym = yMax;} if (ym <= 0) {ym = 0;}
						}
						$(elemMove).css({'left':xm,'top':ym});
					}
				});
			}
			yRotaryControl = y+distanceY;
		}
		if (rotaryL) {
			if (!yRotaryControl) {yRotaryControl = sEly;}
			if (yRotaryControl+(elH/2) >= rotaryLine) {
				if (resizeControl) {$(elem).find(rotaryBlock).css({'transform':'rotate('+rotaryHoriz+'deg)'});}
				else {$(elem).closest(rotaryBlock).css({'transform':'rotate('+rotaryHoriz+'deg)'});}
			}else {
				if (resizeControl) {$(elem).find(rotaryBlock).css({'transform':'rotate('+rotaryNone+'deg)'});}
				else {$(elem).closest(rotaryBlock).css({'transform':'rotate('+rotaryNone+'deg)'});}
			}
		}
	}),false;
}

// Resize Block
function resize_block(data){ // функция изменения размеров блока
	var elem = data['elem'];
	var sx = data['sx'];
	var sy = data['sy'];
	var Ww = data['Ww']; // ширина главного окна
	var Wh = data['Wh']; // Высота главного окна
	var contentBlock = data['contentBlock'];
	var ew,eh,x,y,lx,ly,xMax,yMax,kofRes,resizeD;
	
	var elemMove = data['blockElem'];
	if (!elemMove){elemMove = elem;}
	
	
	if (contentBlock == 'img') {
		//kofRes = data['elH'] / data['elW'];
		//maxElemH = maxElemH*kofRes;
	}
	
	$(elem).off('touchmove');
	$(elem).on('touchmove',function(event) { // если тянем 2мя пальцами
		if (event.targetTouches.length == 2) {
			var touchA = event.targetTouches[0];
			var touchB = event.targetTouches[1];
			ew = Math.round(Math.abs(touchA.pageX - touchB.pageX)-sx);
			eh = Math.round(Math.abs(touchA.pageY - touchB.pageY)-sy);
			
			if (contentBlock == 'img') {
				resizeD = (eh+ew);
				x = resizeD+data['elW'];
			}
			else {y = eh+data['elH']; x = ew+data['elW'];}
			
			lx = data['sElx']-(ew/2);
			ly = data['sEly']-(eh/2);
							
			if (x <= minElemW) {x = minElemW;} if (x >= maxElemW) {x = maxElemW;}
			if (y <= minElemH) {y = minElemH;} if (y >= maxElemH) {y = maxElemH;}
			
			xMax = Math.round(Ww-lx);
			yMax = Math.round(Wh-ly);

			if (lx <= 0) {lx = 0;} if (x >= xMax) {x = xMax;}
			if (ly <= 0) {ly = 0;} if (y >= yMax) {y = yMax;}

			$(elemMove).css({'height':y,'width':x});
			$(elemMove).css({'left' : lx+'px','top':ly+'px'});
		}
	}),false;
	
	$(elem).off('touchend');
	$(elem).one('touchend',function(event) {
		$(elem).off('touchmove');
		
	}),false;
}

// функция распечатки одного окна
function print_one_window(objectPrint){
var elem = (this)?this:objectPrint;
var allButton ='',newWindow;
var winTop = (elem.top)? elem.top:0;
var winLeft = (elem.left)? elem.left:0;
var winWidth = (elem.width)? elem.width:winWidthCo;
var winHeight = (elem.height)? elem.height:winHeightCo;
var winRotary = (elem.rotary)? elem.rotary:0;
var clControlBut,showtext;
var VIPbias = 'left: calc(50% - 25px);';
var VIPClass = '',VIPhellpBias = '';
(gDataInfo.windowIter)?gDataInfo.windowIter++:gDataInfo.windowIter = 1;

if (!allWindowCo){
	if (autoWindowCo) { // если есть колличесвто окошек считаем их положение, и размер автоматически
		var sumWindow = Object.keys(windowCreate).length;
		winHeight = fulWindowHeight/lineWindowCo;
		winWidth = fulWindowWidth/(sumWindow/lineWindowCo);
	}
}

if (rotaryL) {
	if (winTop < rotaryLine) {
		winRotary = 180;
	}
}
if (elem.objects){
	$.each(elem.objects,function(){
		var cloneControl=1,cloneInfo;
		var obTop = (this.top)? this.top : 0;
		var obLeft = (this.left)? this.left : 0;
		var obShow = (this.show)? this.show : 'img/icon/none-img.png';
		var obType = (this.type)? this.type : 'content';
		var obContent = '',dopClassB='';
		var obSrc = (this.src)? 'data-src='+'"'+this.src+'"' : ''; 
		var obRotary = (this.rotary)? 'data-rotary="'+this.rotary+'"' : 'data-rotary="'+winRotary+'"';
		if (this.position) {
			var obPosTop = (this.position.top)? 'data-top="'+this.position.top+'"' : 'data-top="0"';
			var obPosLeft = (this.position.left)? 'data-left="'+this.position.left+'"' : 'data-left="0"';
		} else {obPosTop = ''; obPosLeft='';}
		var obBorder = (this.border)? 'data-border="'+this.border+'"' : '';
		var obBack = (this.back)? 'data-back="'+this.back+'"' : '';
		
		if (obType == 'content') {obContent = this.src; obSrc = '';}
		if (obType == 'background') {clControlBut = butBС.substr(1); dopClassB = butBBС.substr(1)} 
		if (obType == 'help'){
			clControlBut = butHelpWind.substr(1)+' '+butHelpViewWind.substr(1);//доп классы для кнопки
			cloneControl=0;// отключим контент в кнопке
			dopClassB = butHelpWind.substr(1);// доп классы для текста
		}
		else {clControlBut = butСС.substr(1);}
		
		if (this.showtext) {
			var textinfoTop = (this.showtext.top)?this.showtext.top:0;
			var textinfoLef = (this.showtext.left)?this.showtext.left:0;
			if (this.showtext.type == 'back') {}
			var showtext = '<div class="'+showDopInfText.substr(1)+' '+dopClassB+'" style="top:'+textinfoTop+'px; left:'+textinfoLef+'px;">'+this.showtext.text+'</div>';
		}
		else {showtext = '';}
		
		if (cloneControl) {
			cloneInfo =''+ 
				'<div class="'+cloneContent.substr(1)+'"'+
					'data-type="'+obType+'"'+
					obSrc+obRotary+obPosTop+obPosLeft+obBorder+obBack+
					'>'+
				obContent+
				'</div>';
		}else {cloneInfo='';}
		
		allButton = allButton+
			'<div class="'+clCenMinBut.substr(1)+'" style="top:'+obTop+'px; left:'+obLeft+'px;">'+
				'<div class="'+clControlBut+'">'+
					'<div class="'+clCreConInfo.substr(1)+'">'+
						obShow+
					'</div>'+
					cloneInfo+	
				'</div>'+
				showtext+
			'</div>'+
			'';
	});	
}

if (allWindowCo == 1 && butRotateVip) { // вип окно ( для 1 одного пользывателя )
	VIPbias = 'left: calc('+VIPbiasBut+'% - 25px);';
	VIPhellpBias = 'left: calc('+VIPbiasBut+'% - '+(HWVSizW/2)+'px);';
	VIPClass = vipDopHelpClass.substr(1);
	if (butRotateVip.showtext) {
			var textinfoTop = (butRotateVip.showtext.top)?butRotateVip.showtext.top:0;
			var textinfoLef = (butRotateVip.showtext.left)?butRotateVip.showtext.left:0;
			var showtextVip = '<div class="'+showDopInfText.substr(1)+'" style="top:'+textinfoTop+'px; left:'+textinfoLef+'px;">'+butRotateVip.showtext.text+'</div>';
		}
	else {showtext = '';}

	allButton = allButton+
	'<div class="'+clCenMinBut.substr(1)+'" style="top:'+butRotateVip.top+'px; left:'+butRotateVip.left+'px; ">'+
		'<div class="'+vipRotateBut.substr(1)+'">'+
			'<div class="'+clCreConInfo.substr(1)+'">'+
				butRotateVip.show+
			'</div>'+						
		'</div>'+
		showtextVip+
	'</div>'+
	'';
}

winWidth = winWidth+fulWindowTypeSize;
winHeight = winHeight+fulWindowTypeSize;
winTop = winTop+fulWindowTypeSize;
winLeft = winLeft+fulWindowTypeSize;

newWindow =''+//1 окно пользывательского управления
	'<div id="window_'+gDataInfo.windowIter+'" class="'+windowControl.substr(1)+'" style="top:'+winTop+'; left:'+winLeft+'; transform:rotate('+winRotary+'deg); width:'+winWidth+'; height:'+winHeight+';">'+
		'<div class="'+centralBut.substr(1)+'" style="bottom:'+dropUp+'px; '+VIPbias+'">'+
		'<div class="'+cenButCli.substr(1)+'"></div>'+
			allButton+
		'</div>'+
	'</div>'+
	// окно помощи (которое будет закрывать окно управления если что )
	'<div id="help_window_'+gDataInfo.windowIter+'" class="'+HelpWind.substr(1)+' '+HelpViewWind.substr(1)+' '+VIPClass+'" style="position:absolute; top:'+winTop+'; left:'+winLeft+'; transform:rotate('+winRotary+'deg); width:0px; height:0px;">'+
		'<div class="'+HelpWindContent.substr(1)+' '+HelpWindViewContent.substr(1)+'" style="position:absolute; height:0%; width:0%;">'+
			'<div class="'+HelpWindViewContentSiz.substr(1)+'" style="width:'+HWVSizW+'px; height:'+HWVSizH+'px; '+VIPhellpBias+'">'+HelpWindContentCont+
			'<div class="'+butHelpCloWind.substr(1)+' '+butHelpCloViewWind.substr(1)+'"><span></span></div>'+
			'</div>'+
		'</div>'+
	'</div>';
'';
$(windo).append(newWindow);
}

// функция создания страници
function create_page() { 
	$(windo).css({'width':fulWindowWidth+fulWindowTypeSize,'height':fulWindowHeight+fulWindowTypeSize});
	if (allWindowCo){
		var whileI = 0;
		var winHeight = fulWindowHeight/lineWindowCo; // высота одного окна
		var winWidth = fulWindowWidth/(allWindowCo/lineWindowCo); // ширина одного окна
		var sumElemLine = allWindowCo/lineWindowCo; // сумма элементов в 1й линии
		var winTop=0,winLeft=0,controlline=0;
		var newObject = {
			'width':winWidth,
			'height':winHeight,
			'objects':objectsButton
		};
		
		while (controlline < lineWindowCo) {
			var elemI = 0;
			newObject.top = winTop;
			
			while (elemI < sumElemLine) {
				newObject.left = winLeft;
				winLeft = winLeft+winWidth;
				print_one_window(newObject);
				elemI++;
			}
			
			winLeft = 0;
			winTop = winTop+winHeight;
			controlline++;
		}
	}
	else { $.each(windowCreate,print_one_window); }
}

// функция создания нового блока на поле
function add_new_block(elem){
if(elem.length==1){
	var winCont = $(elem).closest(windowControl)
	var topWin =  $(winCont).position().top;
	var lefWin = $(winCont).position().left;
	var widthWin = winCont[0].offsetWidth;
	var heightWin = winCont[0].offsetHeight;
	var rotatefWin = detect_rotate($(winCont).css('transform'));
	var cloneInfo = $(elem).find(cloneContent);
	var dataInfo = cloneInfo.data();
	var elemWidth = (blockWidth)?blockWidth:200;
	var elemHeight = (blockHeight)?blockHeight:200;
	var elemTop=0,elemLeft=0,specClass='block-object',pastContent,cloneElem,RBC,border,background,idNamme,createBlock,styleOpen;

	if (butCCpos == 'window') { // создание относительно окна
		if (rotatefWin == 0){ // если поворот окна 0 градусов
			elemTop = (dataInfo.top) ? (dataInfo.top+topWin):0;
			elemLeft = (dataInfo.left) ? (dataInfo.left+lefWin):0;
		}

		if (rotatefWin == 180) { // если поворот окна 180 градусов
			elemTop = (dataInfo.top) ? (heightWin-elemHeight-dataInfo.top+topWin):0;
			elemLeft = (dataInfo.left) ? (widthWin-elemWidth-dataInfo.left+lefWin):0;
		}
	}
	
	if (butCCpos == 'button') {// создание относительно кнопки
		var butMid = $(elem).closest(centralBut);
		var butMidTop = butMid[0].offsetTop;
		var butMidLeft = butMid[0].offsetLeft;
		if (rotatefWin == 0){// если поворот окна 0 градусов
			elemTop = (dataInfo.top) ? (dataInfo.top+topWin+butMidTop):0;
			elemLeft = (dataInfo.left) ? (dataInfo.left+lefWin+butMidLeft):0;
		} 
		if (rotatefWin == 180) {// если поворот окна 180 градусов
			elemTop = (dataInfo.top) ? (heightWin-elemHeight-dataInfo.top+topWin-butMidTop):0;
			elemLeft = (dataInfo.left) ? (widthWin-elemWidth-dataInfo.left+lefWin-butMidLeft):0;
		} 
	}

	if (dataInfo.type) {
		if (dataInfo.rotary) {RBC = dataInfo.rotary;}
		else {RBC = rotatefWin;}
		if (dataInfo.type == 'content') {
			createBlock = true;
			pastContent = $(elem).find(cloneInfo).html();
		}
		else if (dataInfo.type == 'img') {
			if (dataInfo.src) {
				specClass = 'block-img';
				pastContent = ''+
					'<div class="'+block.substr(1)+' '+specClass+'" style="top:'+elemTop+'px; left:'+elemLeft+'px; width:'+elemWidth+'px; height:'+elemHeight+'px;" data-control2c="1" data-content="img">'+
						'<div class="'+rotaryBlock.substr(1)+'" style="transform: rotate('+RBC+'deg);">'+
							'<div class="'+blockTarget.substr(1)+'">'+
								'<img src="'+dataInfo.src+'">'+
								'<div class="'+blockDelete.substr(1)+'">×</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'';
			}
		}
		else if (dataInfo.type == 'iframe') {
			if (dataInfo.src) {
				createBlock = true;
				pastContent = '<iframe src="'+dataInfo.src+'"></iframe>';
			}
		}
		else if (dataInfo.type == 'video'){
			createBlock = true;
			pastContent = ' <video controls="controls">\
								<source src="'+dataInfo.src+'">\
							</video>'
		}
		else if (dataInfo.type == '3d') {
			createBlock = true;
			id3D++;
			idNamme = 'canvas3d_'+id3D;
			pastContent = '<div class="'+canvas3d.substr(1)+'" id="'+idNamme+'"><div class="preloader3d"><p><span></span></p><h4></h4></div></div>';
			
		}
	}
	
	if (dataInfo) {
		if (dataInfo.border) {border = 'border:'+dataInfo.border+';';} else {border="";}
		if (dataInfo.back) { background = 'background:'+dataInfo.back+';';}else {background="";}
	}
	
	if (createBlock == true) {
		if (typeOpen == 'top') {styleOpen = 'style="top:0px; left:0px; width:100%; height:0px;"';}
		if (typeOpen == 'right') {styleOpen = 'style="top:0px; right:0px; width:0%; height:100%;"';}
		cloneElem = '<div class="'+block.substr(1)+' '+specClass+'" style="top:'+elemTop+'px; left:'+elemLeft+'px; width:'+elemWidth+'px; height:'+elemHeight+'px; '+border+' '+background+'">'+
		'<div class="'+rotaryBlock.substr(1)+'" style="transform: rotate('+RBC+'deg);">'+
			'<div class="'+blockTarget.substr(1)+' '+blockTargetV.substr(1)+'"></div>'+
			'<div class="'+targetOpen.substr(1)+'">'+arreyOpen+'</div>'+
			'<div class="'+blockDelete.substr(1)+'">×</div>'+
			'<div class="'+blockResize.substr(1)+' '+blockOpen.substr(1)+' '+blockTarget.substr(1)+'" '+styleOpen+'><img src="'+blockOpenImg+'"></div>'+
			'<div class="'+bloсkContent.substr(1)+'">'+
		pastContent+'</div></div></div>';
		$(windo).append(cloneElem);
	
		if (dataInfo.type == '3d' && dataInfo.src) {Load_Canvas_3D(idNamme,dataInfo.src);}
	}
	else {$(windo).append(pastContent);}
}
}

// поднимает опускает центральную кнопку,принимает объект кнопки "central-but"
//
function remove_button (elem,spcontrol=0){	
	var posBot = $(elem).css('bottom');
	var dopinfotext = $(elem).find(showDopInfText);
	
	var timeStart = new Date().getTime(); // время клика на объект
	var index = $(elem).closest(windowControl).index();
	var timeEnd = (gDataInfo['time_central_but_'+index])?gDataInfo['time_central_but_'+index]:1;
	
	function buttonUp(){
		$(elem).animate({"bottom": dropDown+'px'},ContTimAnim,"linear",function(){
			$(elem).find(clCenMinBut).css({'display':'block'});
			$(elem).find(clCenMinBut).animate({'opacity': '1.0'},ContTimAnimMin,"linear");
			if(disapText){$(dopinfotext).delay(showDopInfTextTime).animate({'opacity': '0.0'},showDopInfTextTimeOp,"linear",function(){
				$(dopinfotext).css({'display':'none'});
			})};
		});
	}
	
	function buttonDown (){
		$(elem).find(clCenMinBut).animate({'opacity': '0.0'},ContTimAnim,"linear",function(){
			$(elem).find(clCenMinBut).css({'display':'none','opacity': '0.0'});
		});
		$(elem).delay(ContTimAnim).animate({"bottom": dropUp+'px'},ContTimAnim,"linear",function(){
			$(dopinfotext).css({'opacity': '1.0','display':'block'});
		});
	}
	
	if (posBot == dropUp+'px' && spcontrol==0 && timeStart-timeEnd > ContTimDelayClickUw) {  //опускаем
		gDataInfo['time_central_but_'+index] = timeStart;
		buttonUp();
	}
	else if (timeStart-timeEnd > ContTimDelayClickDp){ //поднимаем
		gDataInfo['time_central_but_'+index] = timeStart;
		buttonDown();
	}
}

//Поднимимт кнопку вверх и запустит рекламу если система неактивна
function ad_window (){
	var allWindow = $(windowControl); // получим все окна
	setInterval(function(){
		$(allWindow).each(function (index){ // переберем все окна
			var indexEl = $(this).index(); // получим индекс элемента
			var activia = 0;
			
			if (!gDataInfo['window_'+indexEl]){gDataInfo['window_'+indexEl]=1;}
			
			if (gDataInfo['window_'+indexEl] == 'none'){// выполнить елси окно неактивно
				var elem = $(this).find(centralBut);
				remove_button(elem,1);// вернем кнопку в исходное положение
			}

			gDataInfo['window_'+indexEl] = 'none';
			$(this).off('click');$(this).off('mousemove');
			$(this).off('touchstart');$(this).off('touchmove');
			$(this).one('click mousemove touchmove touchstart',function(event){
				activia++;
				gDataInfo['window_'+indexEl] = activia;
			})
		});
	},returnButtonTime);
}

// функция  реклаы если система неактивна
function commercial(){
	//var addInfoCom = '<div class="'+comClass.substr(1)+'">'+comContent[0].cont+'</div>';
	var addInfoCom = '<div class="'+comClass.substr(1)+'" style="top:0%; left:0%; width:100%; height:100%;"></div>';
	//var content = 
	var oneTime = 0,queueCom,contIter=0,content,topK=0,botK=0,controlTick=0;
	
	//$(windo).prepend(addInfoCom);
	
	function tick(){
		if ($(comClass).length >= 1) {
			(comContent[contIter+1])?contIter++ : contIter=0;
			content = '<div class="'+comClassCont.substr(1)+'">'+comContent[contIter].cont+'</div>';
			$(comClass).html(content);
			oneTime = comContent[contIter].time;
			queueCom = setTimeout(tick, oneTime);
		}
	}
	setInterval(function(){// функция запускающая рекламную часть
	var activia = 0,clickY=0,topK=0,botK=0;
	
	if (!gDataInfo['window_content']){gDataInfo['window_content']=1;}
	
	if (gDataInfo['window_content'] == 'none'){// выполнить елси окно неактивно
		var testCom = $(comClass);
		if (testCom.length == 0){
			$(block).each(function(){
				var elemId3d = $(this).find(canvas3d).attr("id");
				if (elemId3d) { // если ИД 3д существует, сначала выгружаем 3д 
					b4w.require(elemId3d,elemId3d).unload_cb();
				}
			})
			$(block).remove();
			//убираем все окна хелпа HelpWindContent
			$(butHelpCloWind).css({'display':'none'});
			$(HelpWindContent).css({'opacity':'0.0'});
			$(HelpWind).css({'width':'0px','height':'0px'});
			// - убираем все окна хелпа -
			$(windowControl).css({'background':'none'});
			$(windo).prepend(addInfoCom);
			$(comClass).css({'display':'block'});
			$(centralBut).each(function(){
				remove_button(this,1);
			}); 
			$(comClassMid).off('click touchstart');
			$(comClassMid).one('click touchstart',function(event){ 
				$(comClass).css({'display':'none'});
				$(comClass).remove();
				clearTimeout(queueCom);contIter=0;
			});
			tick();	
		}
	}
	else { // если активно
		gDataInfo['window_content'] = 'none';
		$(comClassMid).off('click touchstart touchmove mousemove');
		$(comClassMid).one('click touchstart touchmove mousemove',function(event){
			$(comClass).css({'display':'none'});
			$(comClass).remove();
			activia++;contIter=0;
			gDataInfo['window_content'] = activia;
		});
	}
		

	},commercTime); 
}

// Административная панелль контроля
function admin_control_panel(){
	var addAdminWindow,controlDisp;
	
	function admin_control_panel_opcl (){
		controlDisp = $(adminControlWindowClass).css('display');
		if (controlDisp == 'block') {$(adminControlWindowClass).css({'display':'none','left':'-100px'});}
		if (controlDisp == 'none') {
			$(adminControlWindowClass).css({'display':'block'});
			$(adminControlWindowClass).animate({"left":0+'px'},adminControlWindowTimerAnim);
		}
	}
	
	doc.onkeydown = function(e) {
		e = e || window.event;
		if (e.ctrlKey && e.keyCode == 81) {
			admin_control_panel_opcl();
		}
    return true;
	}
	
	
	$('body').on('touchstart click',adminControlWindowClassFO,function(event){
		admin_control_panel_opcl();
	});
	
	var sumclick = 0;
	$(windo).on('touchstart click',adminControlWindowClassF,function(event){
		var tt = (event.targetTouches)?event.targetTouches.length:0; // колочество пальцев на объекте
		if (tt >= adminControlWindowFinger || sumclick >= adminControlWindowFinger) {
			admin_control_panel_opcl()
			sumclick = 0
		}
		sumclick++; if (sumclick == 1){ setTimeout(function () {sumclick=0;},adminControlWindowTimer);}
	});
		
	$(windo).append('<div class="'+adminControlWindowClassF.substr(1)+'"></div>');
		
	addAdminWindow = ''+
	'<div class="'+adminControlWindowClass.substr(1)+'" style="bottom:100px; left:-100px;">'+adminControlWindowContent+'</div>';//+
	//'<div class="'+adminControlWindowClass.substr(1)+'" style="top:0px; right:0px;">'+adminControlWindowContent+'</div>'+
	//'<div class="'+adminControlWindowClass.substr(1)+'" style="bottom:0px; left:0px;">'+adminControlWindowContent+'</div>'+
	//'<div class="'+adminControlWindowClass.substr(1)+'" style="bottom:0px; right:0px;">'+adminControlWindowContent+'</div>';
	$('body').append(addAdminWindow);
}
	
	
